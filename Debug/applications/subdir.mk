################################################################################
# 自动生成的文件。不要编辑！
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../applications/display.c \
../applications/esp8266api.c \
../applications/gui.c \
../applications/main.c \
../applications/mnt.c \
../applications/oled.c 

OBJS += \
./applications/display.o \
./applications/esp8266api.o \
./applications/gui.o \
./applications/main.o \
./applications/mnt.o \
./applications/oled.o 

C_DEPS += \
./applications/display.d \
./applications/esp8266api.d \
./applications/gui.d \
./applications/main.d \
./applications/mnt.d \
./applications/oled.d 


# Each subdirectory must supply rules for building sources it contributes
applications/%.o: ../applications/%.c
	riscv64-unknown-elf-gcc  -DDEBUG -I"D:\soft\work\editor\RT-ThreadStudio\workspace\ab32_rtt\rt-thread\include\libc" -I"D:\soft\work\editor\RT-ThreadStudio\workspace\ab32_rtt" -I"D:\soft\work\editor\RT-ThreadStudio\workspace\ab32_rtt\applications" -I"D:\soft\work\editor\RT-ThreadStudio\workspace\ab32_rtt\board" -I"D:\soft\work\editor\RT-ThreadStudio\workspace\ab32_rtt\libcpu\cpu" -I"D:\soft\work\editor\RT-ThreadStudio\workspace\ab32_rtt\libraries\hal_drivers\config" -I"D:\soft\work\editor\RT-ThreadStudio\workspace\ab32_rtt\libraries\hal_drivers" -I"D:\soft\work\editor\RT-ThreadStudio\workspace\ab32_rtt\libraries\hal_libraries\ab32vg1_hal\include" -I"D:\soft\work\editor\RT-ThreadStudio\workspace\ab32_rtt\libraries\hal_libraries\ab32vg1_hal" -I"D:\soft\work\editor\RT-ThreadStudio\workspace\ab32_rtt\libraries\hal_libraries\bmsis\include" -I"D:\soft\work\editor\RT-ThreadStudio\workspace\ab32_rtt\libraries\hal_libraries\bmsis" -I"D:\soft\work\editor\RT-ThreadStudio\workspace\ab32_rtt\packages\bluetrum_sdk-latest" -I"D:\soft\work\editor\RT-ThreadStudio\workspace\ab32_rtt\rt-thread\components\drivers\include" -I"D:\soft\work\editor\RT-ThreadStudio\workspace\ab32_rtt\rt-thread\components\finsh" -I"D:\soft\work\editor\RT-ThreadStudio\workspace\ab32_rtt\rt-thread\components\libc\compilers\common" -I"D:\soft\work\editor\RT-ThreadStudio\workspace\ab32_rtt\rt-thread\components\libc\compilers\newlib" -I"D:\soft\work\editor\RT-ThreadStudio\workspace\ab32_rtt\rt-thread\include" -isystem"D:\soft\work\editor\RT-ThreadStudio\workspace\ab32_rtt" -include"D:\soft\work\editor\RT-ThreadStudio\workspace\ab32_rtt\rtconfig_preinc.h" -std=gnu11 -c -mcmodel=medany -march=rv32imc -mabi=ilp32 -msave-restore -Os -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "$@" "$<"

