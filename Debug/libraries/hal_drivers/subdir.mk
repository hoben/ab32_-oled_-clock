################################################################################
# 自动生成的文件。不要编辑！
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../libraries/hal_drivers/drv_common.c \
../libraries/hal_drivers/drv_gpio.c \
../libraries/hal_drivers/drv_rtc.c \
../libraries/hal_drivers/drv_usart.c 

OBJS += \
./libraries/hal_drivers/drv_common.o \
./libraries/hal_drivers/drv_gpio.o \
./libraries/hal_drivers/drv_rtc.o \
./libraries/hal_drivers/drv_usart.o 

C_DEPS += \
./libraries/hal_drivers/drv_common.d \
./libraries/hal_drivers/drv_gpio.d \
./libraries/hal_drivers/drv_rtc.d \
./libraries/hal_drivers/drv_usart.d 


# Each subdirectory must supply rules for building sources it contributes
libraries/hal_drivers/%.o: ../libraries/hal_drivers/%.c
	riscv64-unknown-elf-gcc  -DDEBUG -I"D:\soft\work\editor\RT-ThreadStudio\workspace\ab32_rtt\rt-thread\include\libc" -I"D:\soft\work\editor\RT-ThreadStudio\workspace\ab32_rtt" -I"D:\soft\work\editor\RT-ThreadStudio\workspace\ab32_rtt\applications" -I"D:\soft\work\editor\RT-ThreadStudio\workspace\ab32_rtt\board" -I"D:\soft\work\editor\RT-ThreadStudio\workspace\ab32_rtt\libcpu\cpu" -I"D:\soft\work\editor\RT-ThreadStudio\workspace\ab32_rtt\libraries\hal_drivers\config" -I"D:\soft\work\editor\RT-ThreadStudio\workspace\ab32_rtt\libraries\hal_drivers" -I"D:\soft\work\editor\RT-ThreadStudio\workspace\ab32_rtt\libraries\hal_libraries\ab32vg1_hal\include" -I"D:\soft\work\editor\RT-ThreadStudio\workspace\ab32_rtt\libraries\hal_libraries\ab32vg1_hal" -I"D:\soft\work\editor\RT-ThreadStudio\workspace\ab32_rtt\libraries\hal_libraries\bmsis\include" -I"D:\soft\work\editor\RT-ThreadStudio\workspace\ab32_rtt\libraries\hal_libraries\bmsis" -I"D:\soft\work\editor\RT-ThreadStudio\workspace\ab32_rtt\packages\bluetrum_sdk-latest" -I"D:\soft\work\editor\RT-ThreadStudio\workspace\ab32_rtt\rt-thread\components\drivers\include" -I"D:\soft\work\editor\RT-ThreadStudio\workspace\ab32_rtt\rt-thread\components\finsh" -I"D:\soft\work\editor\RT-ThreadStudio\workspace\ab32_rtt\rt-thread\components\libc\compilers\common" -I"D:\soft\work\editor\RT-ThreadStudio\workspace\ab32_rtt\rt-thread\components\libc\compilers\newlib" -I"D:\soft\work\editor\RT-ThreadStudio\workspace\ab32_rtt\rt-thread\include" -isystem"D:\soft\work\editor\RT-ThreadStudio\workspace\ab32_rtt" -include"D:\soft\work\editor\RT-ThreadStudio\workspace\ab32_rtt\rtconfig_preinc.h" -std=gnu11 -c -mcmodel=medany -march=rv32imc -mabi=ilp32 -msave-restore -Os -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "$@" "$<"

