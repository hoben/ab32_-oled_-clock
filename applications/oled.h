#ifndef __OLED_H__
#define __OLED_H__

#include "delay.h"
#include "board.h"
//#include "oledfont.h"

//--------------OLED参数定义---------------------
#define PAGE_SIZE    8
#define XLevelL     0x00
#define XLevelH     0x10
#define YLevel       0xB0
#define Brightness   0xFF
#define WIDTH        128
#define HEIGHT       64

#define     OLED            0
#define     SIZE            16
#define     XLevelL         0x00
#define     XLevelH         0x10
#define     Max_Column      128
#define     Max_Row         64

#define     X_WIDTH         128
#define     Y_WIDTH         64

//IO Definitions
#define     CS_PIN          "PE.0"//PDout(3)    //CS
#define     DC_PIN          "PE.1"//PDout(5)    //DC
#define     RST_PIN         "PA.5"//PDout(4)    //RES
#define     DIN_PIN         "PB.0"//PDout(7)    //D1
#define     CLK_PIN         "PF.1"//PDout(6)    //D0


#define     OLED_CS_Clr()       rt_pin_write(rt_pin_get(CS_PIN), PIN_LOW)//CS = 0  //CS
#define     OLED_CS_Set()       rt_pin_write(rt_pin_get(CS_PIN), PIN_HIGH)//CS = 1

#define     OLED_RST_Clr()      rt_pin_write(rt_pin_get(RST_PIN), PIN_LOW)//RST = 0 //RES
#define     OLED_RST_Set()      rt_pin_write(rt_pin_get(RST_PIN), PIN_HIGH)//RST = 1

#define     OLED_DC_Clr()       rt_pin_write(rt_pin_get(DC_PIN), PIN_LOW)//DC = 0 //DC
#define     OLED_DC_Set()       rt_pin_write(rt_pin_get(DC_PIN), PIN_HIGH)//DC = 1

#define     OLED_CLK_Clr()     rt_pin_write(rt_pin_get(CLK_PIN), PIN_LOW)//CLK = 0//CLK--D0
#define     OLED_CLK_Set()     rt_pin_write(rt_pin_get(CLK_PIN), PIN_HIGH)//CLK = 1

#define     OLED_DIN_Clr()     rt_pin_write(rt_pin_get(DIN_PIN), PIN_LOW)//DIN = 0 //DIN--D1
#define     OLED_DIN_Set()     rt_pin_write(rt_pin_get(DIN_PIN), PIN_HIGH)//DIN = 1

typedef unsigned char u8;
//typedef unsigned char uint8_t ;
typedef unsigned int u32;
typedef unsigned short u16;

typedef enum
{
    OLED_CMD,
    OLED_DATA
}OLED_WR_MODE;

typedef enum
{
    Display_ON,
    Display_OFF,
    Display_Clear,
    Display_Test
}DIS_MODE;

//OLED控制用函数
extern void OLED_WR_Byte(uint8_t dat,OLED_WR_MODE cmd);
extern void OLED_Display_Status(DIS_MODE mode);
extern void OLEDConfiguration(void);
extern void OLED_DrawPoint(uint8_t x, uint8_t y, uint8_t t);
extern void OLED_Fill(uint8_t x1, uint8_t y1, uint8_t x2, uint8_t y2, uint8_t dot);
extern void OLED_ShowChar(uint8_t x, uint8_t y, uint8_t chr);
extern void OLED_ShowNum(uint8_t x, uint8_t y, u32 num, uint8_t len, uint8_t size);
extern void OLED_ShowString(uint8_t x,uint8_t y, uint8_t *p);
extern void OLED_Set_Pos(uint8_t x, uint8_t y);
extern void OLED_ShowCHinese(uint8_t x, uint8_t y, uint8_t no);
extern void OLED_DrawBMP(uint8_t x0, uint8_t y0,uint8_t x1, uint8_t y1, uint8_t BMP[]);

void OLED_Display_On(void);
void OLED_Display_Off(void);
void OLED_Set_Pos(unsigned char x, unsigned char y);
void OLED_Reset(void);
void OLED_Init(void);
void OLED_Set_Pixel(unsigned char x, unsigned char y,unsigned char color);
void OLED_Display(void);
void OLED_Clear(unsigned dat);

#endif
