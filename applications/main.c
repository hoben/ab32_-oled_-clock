/*
 * Copyright (c) 2020-2021, Bluetrum Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2020/12/10     greedyhao    The first version
 */

/**
 * Notice!
 * All functions or data that are called during an interrupt need to be in RAM.
 * You can do it the way exception_isr() does.
 */

#include <rtthread.h>
#include "board.h"



int main(void)
{

    uint8_t pin2 = rt_pin_get("PE.4");
    rt_pin_mode(pin2, PIN_MODE_OUTPUT);
    rt_kprintf("Hello, world\n");

//    time_t mytime;
//    struct tm *p;

//    set_date(2021,12,20);
//    set_time(1,48,1);


    display_init();

    while (1)
    {
//        time(&mytime);
//        p = localtime(&mytime);
//        rt_kprintf("%d-%d-%d %d:%d:%d\n", (1900 + p->tm_year), ( 1 + p->tm_mon), p->tm_mday,
//                                        (p->tm_hour + 12), p->tm_min, p->tm_sec);
       rt_thread_mdelay(1000);

    }
}
