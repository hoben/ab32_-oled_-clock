/*
 * Copyright (c) 2006-2021, RT-Thread Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2021-12-20     hoben       the first version
 */

#include "display.h"
#include "oled.h"
static void display_entry(void *parameter)
{

    time_t mytime;
    struct tm *p;
    set_date(2021,12,20);
    set_time(20,44,1);

    OLED_Display_Status(Display_Clear);
    while(1)
    {
        //OLED_Display_Status(Display_Clear);
        time(&mytime);
        p = localtime(&mytime);
        rt_kprintf("%d-%d-%d %d:%d:%d\n", (1900 + p->tm_year), ( 1 + p->tm_mon), p->tm_mday,
                                        (p->tm_hour + 12), p->tm_min, p->tm_sec);

        GUI_ShowNum(5,10,1900 + p->tm_year,4,16,1);
        GUI_ShowString(35,10,"-",16,1);
        GUI_ShowNum(42,10,1 + p->tm_mon,2,16,1);
        GUI_ShowString(60,10,"-",16,1);
        GUI_ShowNum(67,10, p->tm_mday,2,16,1);

        GUI_ShowNum(20,30,1 + p->tm_hour,2,16,1);
        GUI_ShowString(40,30,":",16,1);
        GUI_ShowNum(50,30,1 + p->tm_min,2,16,1);
        GUI_ShowString(68,30,":",16,1);
        GUI_ShowNum(75,30,1 + p->tm_sec,2,16,1);

        //GUI_ShowString(4,48,"www.lcdwiki.com",16,1);

        //rt_thread_delay(1000);
    }
}

rt_thread_t display=RT_NULL;
int display_init(void)
{
    //OLED_Init();
    OLEDConfiguration();
    //创建显示线程 优先级设置为31
    display = rt_thread_create("display", display_entry, RT_NULL, 1024, 28, 5);
    //启动显示线程
    if(RT_NULL != display)
    {
        rt_thread_startup(display);
        rt_kprintf("display_ok\n");
    }
    return 0;
}
