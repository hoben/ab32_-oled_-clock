/*
 * Copyright (c) 2006-2021, RT-Thread Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2021-12-20     hoben       the first version
 */
#ifndef APPLICATIONS_DISPLAY_H_
#define APPLICATIONS_DISPLAY_H_

//#include "board.h"

int display_init(void);

#endif /* APPLICATIONS_DISPLAY_H_ */
